## low power pelican theme

low power is a pelican theme based on the [Solar](https://github.com/lowtechmag/solar) theme designed for <https://solar.lowtechmagazine.com>. It is an attempt to reduce the energy use the associated with accessing content on a site hosted in GitLab Pages (though it could be used on other hosting platforms as well).  

The theme is designed to be very lightweigh, with minimal scripts and with images dithered by default to reduce their size.  For sites that require high-res images to convey information, you can enable a "Load Full Image" button by setting `DEDITHER = True` in pelicanconf.py.  

This theme relies on a few custom plugins, which are *required* for the theme to work.  They are in the low-power-plugins directory.  

Notable changes from the Solar theme include:
1. Optionally add a "Load Full Image" button below dithered images in posts and pages that replaces the dithered image a full-resolution, full-color, and full-page-width version.
2. Make images height-responsive so that text is always visible above or below dithered images (full images may be taller than the viewport, though)
3. No weather forecast or battery indicator, since this isn't primarily targeted at solar-powered sites.  (I may add that back in later as an option, though?)
4. Top menu is automatically filled with categories and non-hidden pages instead of having hardcoded entries
5. Footer content is mostly pulled from pelicanconf.py

## Installation of dependencies

To generate the website you will need to install some basic utilities. Instructions are for machines based on Debian stretch.

`sudo apt update && sudo apt install git python3-dev python3-setuptools python3-pip zlib1g-dev libjpeg-dev jq`

Installing pelican:

`sudo pip3 install wheel pelican markdown typogrify`

Installing dependencies for the plugins:
`sudo pip3 install Pillow bs4 git+https://www.github.com/hbldh/hitherdither webassets libsass cssmin`

Install the plugins themselves:

`git clone https://github.com/lowtechmag/solar-plugins`

make sure you add that folder to the list of `PLUGIN_PATHS` in `pelicanconf.py`


