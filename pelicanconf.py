#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'D. April Wick'
SITENAME = 'AprilWick.com'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Athens'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Solar Website', 'https://homebrewserver.club/low-tech-website-howto.html'),)

INFO = (('About This Site', "/pages/about-site.html"),
        ('Privacy Policy', "/pages/privacy.html"),
        ('About April', "/pages/about-me.html"),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

PLUGIN_PATHS = (('./low-power-plugins/'),)
#PLUGINS = ['addressable_paragraphs','assets','dither','neighbors','related_posts','representative_image']
PLUGINS = ['dither', 'assets','representative_image','addressable_paragraphs']

THEME = './low-power/low-power/'

DEDITHER = True
