This is the personal website of D. April Wick.  

But also...

This repository can be forked to create GitLab Pages sites using Pelican and the low power theme - see below.  You can also download the contents of this repository and install the dependencies described in `low-power/low-power/README.md` to generate and host the site elsewhere.  

## low power pelican theme

low power is a pelican theme based on the Solar theme designed for <https://solar.lowtechmagazine.com>. It is an attempt to reduce the energy use the associated with accessing content on a site hosted in GitLab Pages (though it could be used on other hosting platforms as well).  

The theme is designed to be very lightweigh, with minimal scripts and with images dithered by default to reduce their size.  For sites that require high-res images to convey information, you can enable a "Load Full Image" button by setting `DEDITHER = True` in pelicanconf.py.  

This theme relies on a few custom plugins, which are *required* for the theme to work.  They are in the low-power-plugins directory.  

Notable changes from the Solar theme include:
1. Optionally add a "Load Full Image" button below dithered images in posts and pages that replaces the dithered image a full-resolution, full-color, and full-page-width version.
2. Make images height-responsive so that text is always visible above or below dithered images (full images may be taller than the viewport, though)
3. No weather forecast or battery indicator, since this isn't primarily targeted at solar-powered sites.  (I may add that back in later as an option, though?)
4. Top menu is automatically filled with categories and non-hidden pages instead of having hardcoded entries
5. Footer content is mostly pulled from pelicanconf.py

### Learn more about GitLab Pages at https://pages.gitlab.io and the [official documentation](https://docs.gitlab.com/ce/user/project/pages/), including[how to fork a project like this one to get started from](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_two.html#fork-a-project-to-get-started-from).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in the file [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Pelican
1. Regenerate and serve on port 8000: `make devserver`
1. Add content

Read more at Pelican's [documentation].

## GitLab User or Group Pages

To use this project as your user/group website, you will need to perform
some additional steps:

1. Rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings > General (Advanced)**.

2. Adjust Pelican's `SITEURL` configuration setting in `publishconf.py` to
the new URL (e.g. `https://namespace.gitlab.io`)

Read more about [GitLab Pages for projects and user/group websites][pagesdoc].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[pelican]: http://blog.getpelican.com/
[install]: https://docs.getpelican.com/en/stable/install.html
[documentation]: http://docs.getpelican.com/
[pagesdoc]: https://docs.gitlab.com/ce/user/project/pages/getting_started_part_one.html
