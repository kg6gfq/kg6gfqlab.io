Title: Dynamo Taillight
Category: Bicycles
Date: 2016-1-14
Tags: bike, electronics, power
Lang: en
Status: published
Summary: A DIY dynamo-powered taillight using a very simple standlight circuit.

![image](./images/dynamo-taillight/1000 lit.jpg)

This taillight works with standard hub and bottle dynamos - nominally 6V 500mA, though they can actually produce much higher voltages depending on the speed and load.  It uses a [Flare optic](http://www.ledil.com/products/?ds=1&fr=&mn=2&ld=289&fm=71&md=&xd=&mh=&xh=&mt=&mf=&xf=&sf=&ty=&sm=&cn=&vc=&sfa=1&io=&tro=&trt=0&tpo=&tpt=0&tr=&tp=&st=&pg=&lo=&ol=&of=&od=&oh=) from LEDiL and is crammed into the housing of an old Union dynamo light.  I developed the circuit with help from several folks in [this thread](http://www.candlepowerforums.com/vb/showthread.php?411171) on the CandlePower Forums, and it's based on earlier [CPF dynamo](http://www.candlepowerforums.com/vb/showthread.php?210602) [light threads](http://www.candlepowerforums.com/vb/showthread.php?366404).  

I built another taillight using the same basic schematic, but with an LED and housing from a USB-rechargeable light that had a bad battery.  There's a photo on the [bolted aluminum utility trike page](https://kg6gfq.gitlab.io/bolted-aluminum-utility-trike.html).

![image](./images/dynamo-taillight/2000 schematic.jpg)

The goal was to produce a simple schematic with only passive components, and I think it's generally a success.  D2 and D3 are generic 5mm "superbright" LEDs, while D4 is a Cree XP-E2 mounted on a "star" printed circuit board.  C1 is a 5.5V 1F supercapacitor.  R1 may or may not need to be rated for 1W - I'm no expert on this stuff.  

![image](./images/dynamo-taillight/3000 board side.jpg)

Substituting components in this circuit can be tricky - it's based on balancing resistor values against the voltage/current curves of the various LEDs.  Check the [CPF thread](http://www.candlepowerforums.com/vb/showthread.php?411171) for some guidance on how to do it, or start your own CPF thread - there are some very knowledgable and helpful folks over there!

![image](./images/dynamo-taillight/4000 led holder.jpg)

I 3D-printed a holder for the LED star board and the LEDiL lens.  It also has a couple of holes for the other LEDs (D2 & D3), on the left in this photo.  

![image](./images/dynamo-taillight/5000 board_inhousing.jpg)

The board just fits into the Union housing.  I printed up an adapter for connecting the back of the housing to the rack, but didn't take any photos before mounting it on the bike.  

![image](./images/dynamo-taillight/6000 light on wall.jpg)

The light output is pretty good, even thought it's somewhat distorted by the ribs on the Union lens.  The below photo was taken with the light pointed at the wall, about six feet away.  

Of course, it isn't perfect.  The main (Cree XP-E2) LED isn't running at anywhere near full power, and the other two LEDs are only on when moving.  The standlight would stay brighter longer with a larger capacitor, but would take up more space.  Running the taillight in series with the headlight, rather than in parallel, would allow it to draw a higher current and a correspondingly higher voltage given sufficient speed.  

If I were to do it again, though, I would probably ditch the XP-E2 altogether.  Looking at taillights recently, the ones I've liked most have been only moderately bright, but have spread their light over a very large area.  A handful of 5mm LEDs with suitable optics would be more cost-effective and likely more visible on the road.  
