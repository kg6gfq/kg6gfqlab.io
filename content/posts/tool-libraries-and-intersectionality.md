Title: Tool Libraries and Intersectionality
Date: 2022-12-20
Category: Essays
Tags: essay, toollibrary
Lang: en
Status: draft
Summary: I love tool libraries, but they can be... problematic sometimes.

![image](./images/bolted-aluminum-utility-trike/front-left.JPG)









* The tilting mechanism comes from the classic [FlevoTrike](http://flevofan.ligfiets.net/)

Below is an overview of the trike from front to back.

## Front End

![image](./images/bolted-aluminum-utility-trike/frontend.JPG)
Image: The front end of the trike - steering and drivetrain are all up here!


