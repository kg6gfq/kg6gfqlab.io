Title: Plot Rescue
Date: 2024-06-21
Category: Writing
Lang: en
Status: hidden
Summary: Chapter 1 of Plot Rescue (this is a very rough draft)


"Hey, could you do me a quick favor?" 

"No." Mads didn't even turn away from her weeding. She knew if it really mattered Eva would keep asking. Eva was probably the closest she had to a friend, and one of the only people who wore a mask around Mads without being asked, even if it was just a thin cotton one. Still, she didn't want to give in too easily. 

"Please? Fucking Adam asked me last minute to come to the office." 

"You need better work-life boundaries. And just because I'm a writer too doesn't mean I want to fill in as a journalist." 

"No! Maddy-Mads, come on. You know that's not what I was going to ask you. I have to go to work because it might be about the promotion. Can you do the tour for the new gardener if I'm not back in time?" 

"No, and don't call me that." Mads stabbed the soil with her trowel a little harder than strictly necessary and levered out another dandelion. 

"Hey, it's just a little garden tour. You can do it in your sleep. Nobody knows this place better than you do." 

"The last person I did a tour for was you. Now you bother me all the time and ask for favors." 

"And you know you secretly love me for it."

Mads dug at a pernicious clump of oxalis. She wasn't about to admit that chatting with Eva was sometimes enjoyable; not when it came with requests like this. 

"Please, Mads? I really fucking need this. I'll trade you a couple hours of weeding?" 

"You know I don't let anyone else work on my plot." 

"Yeah, fine, I know. How about a couple hours on the fenceline next to you? That's not really your plot, but I know you want the crabgrass pulled so it stops encroaching." 

Mads finally wrenched the oxalis free and slammed it into her weeding bucket. "Fine. Try to make it back, but I guess I can start the tour if you're not here in time." 

-------------

Lily Campbell rolled up to the vine-covered chainlink fence, stepped off her bicycle, adjusted her grip on the handlebars, and wondered - not for the first time - if this was really a good idea. Still, she was here. She had filled out the paperwork, paid the (nominal) fee, read the orientation packet in the email. Odds were low anyone here would give her a hard time. She just had to try yet another new thing. It would be fine. She had done harder things than this in the past year. 

She rolled her bike over to the rack, locked it up, grabbed her purse out of the basket, adjusted the surgical mask on her face, and took a deep breath. This was going to be fine. 

Lily was about to take another calming breath when the gate swung open with a creak behind her, and a brusque voice said, "Hey. You're Lily?" She spun to find a woman a little shorter than her own five foot eight, with a mess of curly brown hair pulled back casually behind a bandana. The woman's hazel eyes were bright over a dirt-smudged white facemask. 

"Yes! Hi! Hello. I'm Lily. Sorry, um," she forced a smile and held out a hand to shake. 

The other woman glanced at her extended hand, raised an eyebrow, and just nodded in response before turning back through the gate. "Call me Mads. You're new to the garden right? Have you had a community garden plot before? How much gardening experience do you have?" 

Lily started after her, barely catching the gate before it slammed shut. "Yes, I'm new. This is my first time having a community garden plot, it's my first time gardening, really, I never..." Lily shrugged self-consciously. 

"Right. You're here in plot 14P. James abandoned it last fall but he at least had the decency to let us know, so it's in passable shape now. Frankly, it looks better than it ever did while he was gardening it." Mads pointed at a set of garden beds that looked surprisingly verdant, for a plot that had been abandoned for half a year. 

A small sign on a stick poked up from one corner. Lily bent to read it. "Plot rescue? What does that mean?" 

"It's how we label plots we know are abandoned. Anyone can help maintain the Rescue plots, and it counts toward your volunteer credit. You know how the volunteer credit system works?"

"Yes, I read the intro packet. It was very thorough. There's a logbook in the shed for hours, right?"

"Yes, come on. I'll show you. And make sure you know how to take care of the tools properly." Mads continued talking as she walked toward a small shed at the far end of the garden, bracketed by picnic tables on either side. Lily took the opportunity to give her a once-over: She wore a snug teal tank top over jean shorts that looked a little worse for the wear - not especially ratty by the standards of a college town - but definitely more suited for gardening than an evening out. 

The shorts really did hug her hips nicely, Lily thought, as Mads spun to face her and said "...so you always clean tools - especially shovels - before you put them away, got that?" Lily nodded, which seemed to be enough. "I'll show you the hose arrangement in a minute, but first..." 

It's not that Lily wasn't paying attention to Mads's monologue. She was, really. It's just she had gotten a little distracted watching the way she gestured at everything in the toolshed, and was trying to figure out how much of it was aesthetic appreciation and how much of it was envy, and that was never an easy question for her. 

The whirlwind tour took them through a meager seed catalog, a community bulletin board, and a brief overview of the plants around the border of the garden, which were communally maintained and another possible source of volunteer credit. As was toolshed cleanup, path clearing, weeding in shared spaces, and tool mainenance. 

"Oh, I could probably help with tool maintenance," Lily said. "I mean, uh, what sorts of maintenance? It's just, I helped my dad with a lot of that kind of stuff when I was a kid, so I'm pretty okay at it." Shoot, she was talking too much again. Her voice wasn't up to this much talking; it already nearly gave out on her earlier. 

Mads stared at her for a moment, then glanced down at Lily's floral-print sundress and strappy sandals. It was probably not the best outfit for gardening, but today was just an orientation, right? The sandals were flats, at least. That was a little bit practical. And Lily wanted to look cute, except doing that at a garden probably included looking like you were ready for gardening, not a picnic in the park. Maybe this outfit wasn't such a good idea.

"Yeah, ok," Mads finally replied. "It's mostly tightening bolts and replacing handles, and a couple times a year Eva borrows a few angle grinders and we sharpen everything. We'll probably be doing that in a week or so. You used an angle grinder before?" 

"I can do that! I know how to use a grinder without loosing any fingers." 

Mads cocked her head and blinked. "Right. I'll let you know when we have a date set for it." 

"Awesome! It's a date!" Lily reviewed that sentence in her head and tried not to visibly cringe. "I mean, it's a plan." 

But Mads was already walking away. "Let me show you what's in your plot already, and I'll tell you how to take care of things. Eva's really supposed to be giving you this tour, so ask her if you have any questions in the future. Or Sandy and Jan, or Andy." She pointed at another garden plot, then two more. "They've all helped some with the rescue plots and know their stuff. I can't guarantee that anyone else knows what they're doing." 

Lily's mind was already whirling by the middle of the plot tour, trying to keep track of which big green leaves were collards, which were broccoli, and which were ("maybe," said Mads, "it's saved seed, so I'm not entirely sure") going to be cabbages. There were apparently also some carrots ("but not very many, you'll want to succession-plant those for a month or two yet so you get a continuous yield into early summer") and some kind of gigantic bean that you could eat or use as mulch, and about half a dozen other things she couldn't quite keep track of. She really ought to have brought a notebook. 

"...and these are the raspberries," Mads added, pointing at a shock of tall, leafy canes that filled an entire bed. "You inherited the best berry patch in the garden. James neglected it and it'll need pruning soon, but I can give you some pointers. These have been here even longer than I have, and at this point nearly everyone in the garden has taken cuttings, but you've got the originals and it's the biggest stand of them." 

"Oh, wow," Lily said, wondering what the pruning would entail, "that sounds... berry good?" She smiled weakly at her own pun. 

Mads stared at her a moment, then the corners of her eyes crinkled above her mask and she bent nearly double, snorting with laughter for a moment. "Lily, that was awful." 

"Uh, I'm sorry?" 

Mads huffed another laugh and straightened up, still smiling at her for possibly the first time that afternoon. She had a good smile, Lily thought, even though it was obscured by the mask. "No, it was a terrible joke, but - hah - I still got a good laugh out of it." 

Lily was still appreciating Mads's smile when she realized it was probably her turn to break the awkward silence. "Well, I'm glad you appreciate it, because terrible puns are basically the only humor I'm good at." 

"Dad jokes, huh?" 

Lily winced internally at the term, but only said "Something like that." 

"Hey, by the way, I wanted to thank you for wearing that mask. I know they're just recommended and not required in the garden, but..." Mads looked to the side, seeming uncomfortable for the first time since she'd opened the garden gate. "My immune system isn't always a hundred percent, so. Thanks." 

"Oh, um, of course. I mean..." Lily swallowed. This would be a perfect opening to explain why she was actually wearing the mask, especially since it had clearly cost Mads something to bring it up. She did need to practice telling people, after all, but she hadn't decided for sure if she wanted everyone at the garden to know yet. Not that she'd lie if somebody asked, but this was one of the first places she might be able to avoid bringing it up herself. 

She had just braced herself to say it anyway and damn the consequences when the garden gate slammed open and someone behind her shouted, "Hey, Maddy-Mads, is this the new girl? Hey new girl!"  
