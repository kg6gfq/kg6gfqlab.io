Title: Whike
Date: 2021-07-27
Category: Bicycles
Tags: bike, trike, recumbent, tandem, sail
Lang: en
Status: published
Summary: Wind + Trike = Whike (a sail trike)

![image](./images/whike/bolted-trike-and-whike-loaded.jpeg)
Image: The Whike connected to the rear end of my bolted aluminum trike to create a tandem.

You can find more information about the Whike at [whike.com](http://whike.com/).

I had been interested in trying a linked-delta tandem for some time, and I've been curious about sail trikes even longer.  So when a Whike turned up on Craigslist, we got it.  It's been a lot of fun!  We don't get enough wind for good sailing in Santa Rosa, but the tandem setup is pretty good.  
