Title: Turkish Spindle
Date: 2016-02-01
Category: Crafts
Tags: crafts, spinning, fiber, 3d printing
Lang: en
Status: published
Summary: A while back I wanted to try handspinning.  Of course, being me, I didn't just go buy a spindle or a wheel.  And a regular DIY spindle made from a CD wouldn't do.  No, I had to design and 3D print a Turkish spindle!

![image](./images/turkish-spindle/00 red thread.JPG)

When Angel found me a bag of fiber I started looking into spindles.  I liked the idea of a [Turkish Spindle](http://tutorials.knitpicks.com/wptutorials/turkish-spinning-tutorial/), but found there weren't any good patterns available online for 3D printing them...  so I whipped this up in OpenSCAD.  I've been spinning for several weeks now and it works great!

![image](./images/turkish-spindle/01 parts.JPG)

The spindle prints as two pieces that slide together, with a hole in the center for a pencil or chopstick (the size and shape of the hole can be tweaked in the SCAD file; pencil is the default).  I recommend using a pencil or other non-round shaft; the round chopstick tends to slip after a while.  

![image](./images/turkish-spindle/02 bare spindle.JPG)

Once it's printed, put it together and start spinning!  (I learned from YouTube videos like [this one](https://www.youtube.com/watch?v=6jEQg6PFe04) - it's pretty straightforward once you get the hang of it.)  

Downloads:

* [OpenSCAD file]({attach}turkish-spindle-files/turkish_spindle.scad) for editing
* [STL file]({attach}turkish-spindle-files/turkish_spindle-pencil.stl) for importing into your favorite slicer
* [Thingiverse page](https://www.thingiverse.com/thing:1413103) where you can use the "customizer" webapp to generate an STL file with custom parameters

![image](./images/turkish-spindle/03 full spindle.JPG)

Note from January 2021: I've spun several skeins on these in the past few years and they continue to work well!  
