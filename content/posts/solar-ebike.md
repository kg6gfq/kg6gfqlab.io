Title: Solar Electric Bikes
Date: 2022-12-08
Category: Bicycles
Tags: bike, trike, cargo, recumbent, electric, solar
Lang: en
Status: draft
Summary: I converted our trikes to solar-electric.  Should you?  And how?

![image](./images/bolted-aluminum-utility-trike/front-left.JPG)

About a year ago I converted our trikes (the FlevoTrike and the [Bolted Aluminum Utility Trike](/bolted-aluminum-utility-trike.html)) to e-assist with solar panels for charging.  We rode around 2000 miles (3200km) from Northern California to the Mexican border and back.  We have also used them for local commute/recreational riding.  They are... interesting!  They have some cool benefits and some frustrating limitations, and I will try to give a balanced review.

This is a rough guide. Check out the links below for more detailed info from people who have a lot more experience than I do:

* [Mark Havran](https://solare.bike/) has produced some of the best guides I know of: 
  * His [Technical Stuff](https://solare.bike/technical-stuff/) page collects links to articles he has written and videos by other folks
  * [Designing a safe and efficient solar ebike](https://solare.bike/2020/12/27/designing-a-safe-and-efficient-solar-ebike/) is the comprehensive, detailed guide that I kept coming back to when working on our trikes.

## Should I Ride A Solar E-Bike?

![Flowchart for "Should I put solar panels on it?"  Does it move around?  If yes, Does it have regular chances to recharge or swap batteries?  Yes = Probably not.  If it does not have regular chances to recharge or swap batteries: When running, is it hot to the touch?  Yes = Haha good luck.  No = maybe.  ](https://imgs.xkcd.com/comics/solar_panels.png)
Image: Of course, [XKCD has a flowchart for this](https://xkcd.com/1924/)

XKCD is right - the answer to the solar e-bike question is a solid "maybe", and it depends on your use case.  Here are a few possible scenarios:

### Commuting

Most e-bike commuters should probably skip solar.  It isn't worth hauling a panel around with you if you can charge your battery at one or both ends of the commute.  

Some possible exceptions:

* You commute to work and then ride long distances to locations without electricity during your workday.  You would probably be better off getting a larger battery or keeping a spare charged battery at work, but if you REALLY want to drag a solar panel around, you could.
* You have a really long commute to a location that has no electricity and consistent sun.  If you can, you should probably keep a solar panel at work instead of carrying it, but I guess you could also drag a solar panel around on your bike.

Really, if most of your e-bike riding is for commuting and you want to use solar charging you should probably set up a solar charging station at home and/or at work.  You can refer to the information below on chargers, solar panels, etc., but possibly with cheaper, heavier panels. (I hear you might be able to get a good deal on secondhand panels if you contact a local solar installer - apparently they sometimes have leftovers when they upgrade old systems.)

Incidentally: Don't bother solar-charging your ebike to save money on your power bill.  In most places, charging an e-bike costs almost nothing and solar panels are expensive.  

### Touring

This is where solar shines!  If you want to tour on an e-bike, your options are (1) solar or (2) constantly planning your trip around charging locations.  For someone who plans to stop every night at a hotel or a campground with electricity, option (2) could be fine.  For the rest of us... solar!

## What's it like?

Cool benefits of solar:

* With enough sun, limited headwinds, and reasonably flat terrain, I can ride 40-50 mile days nonstop without plugging into the wall.  It is amazing!
* Even when I have to stop to recharge, being able to charge anywhere with sun is fantastic.  
* Charge other gadgets - laptops, e-readers, phones, blenders (yes, we have a USB rechargable blender)
* Most people who see it are impressed and/or curious.  Sometimes I think amazement even overwhelms the standard "ugh, a bike in my road" reaction that drivers would otherwise have.

Frustrating limitations of solar:

* Fog and clouds.  It's obvious, but I did not realize just how frustrating it would be to get stuck somewhere because there was no sun.  Between Monterrey and San Francisco, we kept having to stop and wait - often for hours - until the batteries were charged.  We detoured to places with power outlests.  We stayed extra days at campgrounds.  We were constantly behind schedule, and sometimes a battery would die halfway up a hill on a highway with no shoulder, which is an extremely unpleasant experience.  
* When you built it yourself, there is no warranty.  If something breaks down, nobody else will fix it for you.  Not many people will even know how it works.  You'll probably figure it out eventually, but it can sure put a dent in your schedule!
* The Travelling Circus Effect: Everywhere you go, people will stop you and ask the same questions.  *Is that really a solar panel?  Do you have a motor?  How fast does it go?  Do you have to pedal?  Did you build that?  How much did it cost?*  Sometimes it is fun.  Other times I really want people to just let me eat my lunch in peace.  Often I wish they would come up with more interesting questions!  

## How does it work?

* Solar panel turns sunlight into electricity
* MPPT charge controller draws the optimal current & voltage from your solar panel and converts it to the correct voltage for charging your battery
* Your battery charges!

There are other ways to do it.  Some people connect an inverter to the solar panel.  The inverter turns low-voltage DC into wall-outlet-voltage AC and you plug the charger that came with your ebike battery into it.  I don't like this, because converting low-voltage DC from the solar panel to high-voltage AC and then back to low-voltage DC for the battery wastes extra energy.  On cloudy days, I do not have energy to waste!  Some people use two batteries - one plugged into the solar panel to charge while you power your bike with the other one.  That could be necessary if the charging port is inaccessible or the battery's internal protective circuits prevent simultaneous charging and discharging or you want more battery capacity than will fit on your bike.  I guess it works fine, but I don't think there is a significant advantage.  

## Watt about all this electrical terminology?

You don't need a degree in electical engineering to DIY a solar e-bike, but it helps to understand a few basic concepts. Here's my rough attempt at explaining them:

* Volts and Amps - These are the two most... fundamental, I guess? measurements of electricity. Explaining them clearly can be a challenge, but I'll try.
  * Volts (V) are the unit of measure for "electrical potential." I tend to thing of this as "how hard the electrons are pushing" or "how strongly the electrons want to get from point A to point B. On e-bikes, higher voltage systems are usually capable of somewhat higher speeds, all else being equal. Most e-bikes (and therefore ebike batteries) are nominally 36, 48, or 52V. I have a 36V system; it is perfectly adequate.
  * Amps or Amperes (A) are the unit of measure for electric current. I think of it as "how much electricity is flowing through the wire" or "how many electrons are flowing through the wire."
* Watts (W) are the unit of electrical power. Well, really wattage is a general measurement of power - cyclists may recognize it because we often measure human power in watts as well. An average cyclist can easily produce 60+ watts with some consistency, and 200+ when sprinting. 
  * Wattage is calculated by multiplying voltage x amperage. You may see this equation written as P=IE. P is for Power (watts), I is ["from the French phrase intensité du courant, (current intensity)"](https://en.wikipedia.org/wiki/Electric_current), E is for Electromotive Potential, AKA Voltage.
  * Your battery will provide a pretty constant voltage (dropping slightly as it discharges, or when under significant load). You can draw a little bit of power (maybe half a watt to run your headlight) or a lot of power (a thousand watts to climb a steep hill at speed). Since the voltage stays (pretty) constant, we can use the P=IE equation to determine that the amperage is changing. (Yay Algebra!) I'm not going to get into this more right now, there are other people better qualified to explain it, and mostly you just need to know that for a given voltage, amps and watts are proportional. 
  * E-bike people often talk about motors in terms of wattage, as in "I upgraded from a 250 watt motor to a 3000 watt motor" or "class 1 & 2 ebikes are limited to 750 watts". It is more complicated than that; Grin Tech has [a very thorough article on the subject](https://ebikes.ca/learn/power-ratings.html).
  * Solar panels are also rated in watts! They are usually watts at peak theoretical output - full sun, perfect angle, optimal temperature - so you should expect to get less power than your panel is rated for. 
* Watt-hours (Wh) an Amp-hours (Ah) - not to be confused with Watts and Amps! These units measure energy production/consumption over time, or storage capacity. 
  * As with watts and amps, Wh and Ah are proportional to each other when voltage is constant. I mostly think in Wh because it allows me to compare the capacity of a 36V battery with that of a 48V battery easily. Wh = Volts x Ah, so Anywhere you see Ah, you can multiply by the voltage to get Wh. If you have Wh and you need Ah, divide by the voltage.
  * A 500 Wh battery, when fully charged, can provide 500W for an hour, or 100W for 5 hours, or 50W for 10 hours. You can charge it in 100 hours at a rate of 5W, or in 10 hours at a rage of 50W. (If you try to charge it in 1 hour at 500W you will probably damage the battery; more on that below.)
  * Wh (or Ah) are most often used in the e-bike world for:
    * Battery capacity. 500Wh is pretty common, 1000Wh (or 1kWh) is very large by e-bike standards, and it is what I currently use on our trikes for touring.
    * Power consumption - One of the most useful things to know when designing a solar charging system is your average Wh/mile - how many Wh you use, on average, to go a mile? This depends entirely on how much you use the assist, but 10-15 Wh/mile seems like a pretty common range for touring. Once you know your Wh/mile, you can multiply it by the miles you go in a day to get a sense of how much power you use each day, and therefore how big your solar panel needs to be.
  * Resistance / Ohms / Ohm's law - This is a pretty fundamental concept in electronics, but I haven't yet found much use for it in solar e-bike design.

## So what parts do I need to make one myself?

Ok, this is where we apply some of the electrical terminology and math to the basic concepts:

### Battery

If you already have an e-bike, you probably already have a battery.  That makes things simpler!  You can base the rest of your system on the battery specs.  

#### Key numbers

These are the things you need to know about your battery before designing a solar charging system

* Battery chemistry - Many ebikes use lithium ion batteries.  Those are fine, but I prefer LiFePO4 (lithium iron phosphate) because they are less flammable and contain fewer heavy metals.  You need to know your battery chemistry because it is a factor in...
* Battery voltage - Your nominal battery voltage is probably a nice round number like 36, 48, or 52.  Note that I said "nominal"!  Actual voltage depends on your battery chemistry and charge state, so you need to know...
  * Minimum voltage (when fully discharged)
  * Maximum voltage (when fully charged)
* Amp-hours (capacity) - Your battery may list capacity in Watt-hours instead.  That is fine!
* Maximum charge rate (amps)

Some of this info may be hard to find.  My battery was cheap and some of the info on the datasheet that came with the battery conflicted with the info on the seller's website! I made educated guesses and/or went with the more conservative number in the case of things like maximum charge rate.

#### Starting from scratch

If you don't already have an e-bike... don't worry too much about matching the battery to your solar system.  You should be able to solar-charge pretty much any e-bike battery.  

* I do recommend Lithium Iron Phosphate (LiFePO4) batteries
* You may also want to look for something with a good BMS (Battery Management System) - that's the circuit that can prevent over-charging, over-discharging, overheating, charging in below-freezing weather, uneven cell charging, and other things that could potentially ruin your battery!  
* E-assists from big-name manufacturures (e.g. Shimano, Bosch) are often easier to get repaired at a local bike shop - the manufacturers train and certify mechanics, and their systems are pretty standardized. On the other hand, they are often designed to prevent DIY tinkering, with batteries that won't charge unless without a "smart pin" or other trick that identifies an officially sanctioned charger. You can circumvent these measures, but it will present a small extra challenge.
* DIY systems from makers like Bafang and Grin Tech are much more amenable to solar charging - they are often design with tinkerers in mind. The downside is that you may be on your own for maintenance and repairs. Not a lot of professional mechanics will be excited to work on your custom e-bike with a Bafang motor, a Grin controller, and an off-brand battery from eBay.

### Solar Panel(s)

Most solar ebikes - mine included - seem to use flexible panels rated for 50-200 watts. 

Physically, the flex panels are a nuisance to design for.  They are meant to be mounted on the solid surface of a campervan or boat, not on a frame.  I built plywood frames with slight curvature for ours, sacrificing a little bit of optimal sun exposure to get a better stiffness-to-weight ratio.  I have also seen people use coroplast or aluminum sheets with holes drilled out to back the panels.  My ideal panel would be something like the [LightLeaf](lightleafsolar.com/) panels that are stiff but weigh about the same as flex panels.  They are pricey, though!  I don't currently know of anyone else making a stiff lightweight panel except really dedicated DIYers - most stiff panels seem to be the heavy glass-fronted ones for stationary use.

Note also that the more the panels flex, the more likely they are to wind up with tiny fractures in the solar cells that will reduce their efficiency.  [Mark Havran](https://solare.bike/) (currently going around the world on a solar recumbent he has been building for 15 years and which looks like something a professional design company would create) lectured me on the subject when we ran into him biking through LA.  Apparently he knows a few people who have run into that problem with insufficiently-supported panels - some to the point that they had to order new panels mid-tour.  Ours have a little flex and seem to be doing ok, but I worry about it sometimes.  

Panel specifications:

* 

### Charge Controller

The charge controller converts power from the solar panel to the correct voltage for charging your battery.  MPPT chargers also do some kind of clever optimization to use your solar panel most efficiently.  I don't understand it very well, so look elsewhere if you want an explanation!

There are a variety of MPPT chargers on the market, but they mostly fall into a few categories:

 * Cheap (~$40) generic chargers - as far as I know, most of these work fine.  You can get them on ebay or elsewhere.  Usually they have an adjustable output voltage so they'll work with several battery voltages.  
 * Mid-range (~$75) elejoy charger - I got these from Grin Tech.  As far as I can tell, they are a slightly-higher-quality version of the adjustable-output generic chargers.  They have been working well so far!
 * Genasun (~$200) - These are the best of the best, designed for solar vehicle racing.  They have a much higher MPPT sampling rate, which means they will re-optimize faster when going from sun to shade and back again, giving you a little bit more power.  Genasun also sells a 100% waterproof version of their chargers.  (I have had no problems with the waterproofing on my elejoy, but it clearly has some possible points of infiltration.)  The downside - apart from the price - is that Genasun chargers are programmed for a single voltage.  If you get a model designed for 36V LiFePO4 batteries, you cannot use it with any other type of battery.

 Things to watch out for:

 * Make sure the maximum output voltage of your charger is as high as the **maximum** voltage of your battery when it is fully charged.  For a nominal "36 volt" LiFePO4 battery, this is around 43.8 volts.  
 * Check the current rating for your charge controller and the peak current for your solar panel(s). Some systems may need multiple charge controllers - I believe this is often the case when using Genasun controllers with multiple panels that are each rated for 100W or more.

## Structural Considerations: Where/how to mount the panel?










Some of the basic features:

* Large cargo bed - can fit oddly shaped objects, and accepts a premade plastic tub that serves as a "trunk"
* Can be built without specialized tools - I did most of the construction with a hacksaw, a drill press, a power drill, and a handful of other common tools.  No welder, lathe, or mill.  No composites.  No jigs.  Mostly standard, commonly-available parts (except the rear axle, but it should be easy to substitute for that).


Below is an overview of the trike from front to back.

## Front End

![image](./images/bolted-aluminum-utility-trike/frontend.JPG)
Image: The front end of the trike - steering and drivetrain are all up here!

