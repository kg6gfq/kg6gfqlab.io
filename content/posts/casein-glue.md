Title: Casein Glue
Date: 2021-10-30
Category: Misc.
Tags: glue, casein, diy, composites
Lang: en
Status: published
Summary: Research & experimentation on casein-based glues, especially for composite construction.

![image](./images/casein-glue/test-bamboo-joint.JPG)
Image: First test joint between two pieces of bamboo made with linen strips & casein glue.

Several months ago I came across Surjan Singh's [post on how to make natural composites](https://surjan.substack.com/p/36-how-to-make-a-natural-composite) using casein glue and linen.  It's part of a series that is well worth reading!

I've always been wary of epoxy and epoxy-based composites.  They're really useful, but they produce toxic fumes when curing, some people develop severe epoxy allergies, and the end result is basically just plastic - not recyclable or biodegradable.  Casein/linen composites (potentially) don't have any of these problems!  We just have to figure out how to make them strong enough and somewhat waterproof for outdoor applications.  

Vacuum bagging - the composite construction technique Singh used - isn't of much interest to me right now, but that's not the only way to use composites!  I'd like to try two other approaches:

* [Bamboo bike](https://en.wikipedia.org/wiki/Bamboo_bicycle) frame construction (which traditionally uses composite-wrapped joints)
* [Cardboard-core composites](https://microship.com/cardboard-core-composites/) (a cheap, biodegradable alternative to foamcore that uses discarded material) (I recently found that furniture stores sometimes discard honeycomb cardboard!)

So far the only commercial casein glue I've come across is [this one](https://www.clarioncasein.co.in/casein-glue.htm).  Since casein glue has fallen out of favor for things like carpentry and aircraft manufacturing, I'll probably have to DIY it...

## Formulas

Here are some of the more promising casein glue formulas I've found to date:

| **Name** | **Casein** | **Water** | **Lye** | **Lime** | **Ammonia** | **Sodium Silicate** | **Procedure** | **Notes** | **Source** |
|---|---|---|---|---|---|---|---|---|---|
| Casein 2 | 20.51 | 75.3 |  | 4.19 |  |  | "Soak casein in water 4 hrs, then mix with lime" |  | https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7465168/ |
| Casein 3 | 24.01 | 59.07 |  | 16.92 |  |  | "Soak casein in water 4 hrs, then mix with lime" | "probably more waterproof than casein 2, but swells more when wet" | https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7465168/ |
| "Forest Products Lab |  |  |  |  |  |  |  |  |  |
| Formula No. 11" | 100 | 150-250 + 30-50 for lime |  | 20-30 |  | 70 | "Mix casein with water and, in separate container, mix lime with water<br/>When both are mixed, stir lime gently and pour quickly into casein<br/>After ~2 mins when glue starts to thicken, add sodium silicate<br/>Continue stirring 15-20 mins until smooth</br>If too thick, add water.  If too thin, start over.<br/>Pour copper salt in a thin stream & mix in" | "may need 220-230 parts water if casein is less pure<br/>May add 2-3 parts copper chloride or copper sulfate for water & decomposition resistance</br>Sodium Silicate 40° Baume with a silica-soda ratio of from 1 to 3.25<br/>Assumes a high-calcium lime; may need more of lower grade lime" | https://naldc.nal.usda.gov/download/CAT86200199/PDF |
| Simplifier | ?? |  |  |  | ?? |  | "combine casein with ammonia and cover.<br/>Uncover occasionally to stir, should combine thoroughly within 1-2 days" | "I can’t make heads nor tails of the percentages of household ammonia solution vs ammonia vs ammonium hydroxide in these recipes; needs further research<br/>Addition of zinc acetate makes it waterproof (by crosslinking, which may make it non-biodegradable?)<br/>Shelf life up to 1 month (1 week with zinc acetate" | https://simplifier.neocities.org/caseinglue.html |
| Surjan Singh | 100 | 370 | 11 | 20 |  |  | "Add lye to water<br/>Add casein and lime<br/>Stir, then blend until smooth" | Has been successfully used to create vacuum-bagged composite parts | https://surjan.substack.com/p/36-how-to-make-a-natural-composite |

## First Tests - Surjan Singh formula, wrapping bamboo

Test joint: Set two pieces of bamboo at right angles and wrapped with linen strips that have been sized with dilute Surjan Singh formula and then impregnated with undiluted Surjan Singh formula by sandwiching linen and glue between layers of plastic bag and squeegeeing.   Seems quite strong.

## Other Resources

* [Woodworking StackExchange on casein glue](https://woodworking.stackexchange.com/questions/8182/is-there-a-diy-glue-or-epoxy-alternative-strong-enough-to-hold-bamboo-bicycle-fr)
* [1905 book with a lot of vaguely-described formulas for casein glues](https://archive.org/details/caseinitsprepara00scherich/page/n12/mode/1up)
