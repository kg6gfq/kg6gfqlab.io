Title: Python Midracer
Category: Bicycles
Date: 2011-09-16
Tags: bike, recumbent
Lang: en
Status: published
Summary: A center-steered recumbent that I built in Davis.  I toured and commuted on this bike for several years, and it remains one of the most comfortable bikes I've owned.

![image](./images/python-midracer/000 front view.jpg)

This is a variant of the [Python Lowracer](http://www.python-lowracer.de) - a center-steered recumbent that's challenging to master but an absolute joy to ride.  It's the second of three Pythons that I've built, and my favorite so far.  It's now semi-retired and staying at my folks' place until I either repair it or salvage some parts and let it go...  but for a couple of years it was my primary commuter, and may still be the bike I've toured farthest on.

![image](./images/python-midracer/006 by willamette.jpg)

The bike on its maiden voyage - a 600 mile tour through Oregon and California.  The river in the background is the Willamette.

![image](./images/python-midracer/010 main frame jig.jpg)

As frame jigs go, this is pretty flimsy.  Still, it was enough to keep the suspension pivot aligned for welding.  

![image](./images/python-midracer/020 suspension pivot.jpg)

The suspension bracket is just 1/8" mild steel from the scrap bin.  

![image](./images/python-midracer/030 main with rear.jpg)

This rear swingarm is from an old Proflex MTB, probably from the mid-90s, and was meant to work with elastomer suspension.  In retrospect, having it at such a close angle made the frame much more flexible - I should have opened it up for better triangulation.

![image](./images/python-midracer/040 steerer parts.jpg)

If I were to do it again, I would use rod end bearings...  But for this bike I tried a threaded steer tube with caps brazed on each end.  

![image](./images/python-midracer/050 steerer.jpg)

Initially I had bolts going into the nuts that are brazed under the caps, but when one of the caps came out I switched to a threaded rod that went the entire length of the steer tube, keeping it under compression.  

![image](./images/python-midracer/060 front in jig.jpg)

My jig for joining the front frame to the rear was simple, and relied heavily on eyballing alignment against the prototype I had been learning to ride for the past several months.   

![image](./images/python-midracer/070 jig side view.jpg)

A pair of 2x4s for a frame jig.  What could possibly go wrong? 

![image](./images/python-midracer/080 jig rear view.jpg)

What could go wrong? Surprisingly little, as it turns out!  Whether by skill or by luck, the alignment worked out quite well on this build.  

![image](./images/python-midracer/090 disc mount.jpg)

This is the first - and to date, the only - bike I've built with a disc brake.  I made the mount from sheet metal and V-brake bosses.  If I do it again, I'll use something more robust.  This brake always felt squishy, though that may be because I didn't know how to adjust it properly at the time.  

![image](./images/python-midracer/095 seat and rack.jpg)

The seat support and integrated rack are made from angle stock and shelf brackets.  The seat back itself is aluminum, part of an old external frame backpack.  

![image](./images/python-midracer/098 front end.jpg)

![image](./images/python-midracer/100 complete bike.jpg)

I finished much of the build in a rush so I could meet Angel in Portland for a 600 mile tour.  On an untested homebuilt bike, of course!  It worked remarkably well.  
