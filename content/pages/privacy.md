Title: Privacy Policy
Lang: en
Summary: (working on it...)
Slug: privacy
Status: hidden

I personally have no interest in your browsing habits!  I avoid using cookies, JavaScript, and other tools commonly used for tracking in my own site design.  The only JavaScript used on this site is a small script to load the full versions of images when you click the "Load Full Image" buttons.  

As long as this site is hosted on GitLab Pages, I don't have much control over what information their servers record.  It is entirely possible that they or companies they work with aren't particularly respectful of your privacy.  As of January 2021, though, it looks like they're not serving any cookies, scripts, or other content that I did not explicitly include in the design of this site, so that's promising!  
