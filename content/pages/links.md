Title: Links
Lang: en
Summary: Websites and concepts that I want to research further... whenever I have some spare time. 
Slug: links
Status: published

## Power & Heat
* Gasifier info: https://wiki.opensourceecology.org/wiki/Gasifier_Burner (take with grain of salt; these people think cars are necessary for civilization)
* Solar cooking
    * [basket solar cooker w/ can lids](https://www.appropedia.org/Willow_Basket_Parabolic_Solar_Cooker)
    * [another basket solar cooker](https://www.appropedia.org/Willow_Basket_Parabolic_Solar_Cooker)
* [can rocket stove](http://opensourcelowtech.org/rocketstove.html) that looks like a better design than what I've tried in the past.  Site also has a good vertical axis wind turbine design, and suggests getting discarded alu sheet from offset-printing / lithography shops

## Construction
* [Lamellar Roof](https://wiki.opensourceecology.org/wiki/Lamella_Roof) - roof framing w/ cylindrical shape; built from short segments of (usually) dimensional lumber bolted together

## Fabrication & Materials
* [natural composites](https://surjan.substack.com/p/36-how-to-make-a-natural-composite)
* [finishes you can apply while naked](https://blog.lostartpress.com/2020/11/30/rip-the-anarchists-finishing-manual/)
* [database of natural materials](https://materiom.org/) - mostly bioplastics, geopolymers, and stuff that can act as feedstock for 3d printing
* [Speed Tape](https://en.wikipedia.org/wiki/Speed_tape) - aluminum tape for high-airspeed applications

### Origami
* [Origami simulator](https://origamisimulator.org/) and more [Computational Origami Resources](https://langorigami.com/article/computational-origami)
* [Miura (corrugated) fold video](https://www.youtube.com/watch?v=wv8Sa3T__K4)
* [Origami can be hardened with MinWax](https://www.origamitessellations.com/2005/07/origami-and-minwax-wood-hardener/)
* [Tomohiro Tachi](https://tsg.ne.jp/TT/index.html) does research on tesselations, many of them collapsible and/or structural.  See research & software sections, and his [Flickr](https://www.flickr.com/photos/tactom/)
    * Large rigid structures from thick panels made by sandwiching canvas between cardboard: [https://tsg.ne.jp/TT/cg/ThickRigidOrigami_tachi_5OSME.pdf]
* [Stiff structures from collapsible tubes](https://www.youtube.com/watch?v=pzhvqMfYY50)
* [Biopaper](https://www.ericgjerde.com/portfolio/specimens-jerome-foundation-fellowship/) - sounds like kombucha leather, but made by a papermaker (also, pretty transparent) and used in some cases for origami tessalations
* [Samira Boon](https://samiraboon.com/about/) does collapsible origami with [digitally-woven textiles](https://www.designboom.com/architecture/studio-samira-boon-3d-textile-structures-origami-digital-weaving-03-22-2018/), including collaborations with T. Tachi (above) and some work on designs intended for [space](https://earthsky.org/space/using-origami-to-design-habitats-for-moon-mars)
* [An attempt](http://www.iaacblog.com/programs/collapsible-origami/) at using x-form spans made out of cloth with a wooden dowel frame to create collapsible shelters for travelers on the Camino De Santiago
* [options for "giant outdoor origami paper"](https://ask.metafilter.com/354058/Giant-Outdoor-Origami-Paper-or-equivalent)

### Tensegrity
* [tensegrity wheelchair](https://tensegritywiki.com/wiki/Wheelchair) on the tensegrity wiki
* [chair](https://tensegritywiki.com/wiki/6_Strut_Suspended_Chair_by_ETS) partial tensegrity
* [adjustable grip hitch](https://www.animatedknots.com/adjustable-grip-hitch-knot) is a useful knot!  If making a loop of string/rope that you want to adjust, it can be turned around so it works better - not sure how to explain!


## Pedal Power
* [e-waste recycling gizmo](https://www.notechmagazine.com/2012/09/pedal-powered-electronic-waste-recycling-machine.html) - grinds ewaste and separates components w/ magnets and eddy current induction

## Transportation
* [3mules](https://3mules.com/) - guy travels the west coast with his three mules, living outside!
* [$1300 spend on bike lanes in NYC = one quality-adjusted life year](https://www.reuters.com/article/us-health-costbenefit-bike-lanes-idUSKCN11Z23A) when averaged over all city residents.
* [car crashes are the #1 cause of death for children in the US](https://www.nejm.org/doi/full/10.1056/NEJMsr1804754) - where "children" are people under 19.  Car crashes account for 20% of child deaths, with everything firearm-related in second place at 15%.
* "We recognize that efficiency is a function of vehicle weight as compared to its cargo weight - another form of the Golden Ratio.  The principle is simple:  If the cargo weighs more than the vehicle itself - as in a person riding a bicycle - then you are approaching true efficiency.  The trick is to have a vehicle/cargo ratio of LESS THAN 1."  - Bill Stites, designer of the [Truck Trike](http://www.trucktrike.com/who-we-are)

## Bicycles
* [a unique direct-drive bakfiets](https://cwandt.com/products/penny-pelican-cargo-bike)
* [python-style recumbents, including some folding versions](http://sedan.by.seilsteiger.de/)
* [Finnish recumbent builders forum](https://4rzcdm4enzemnizaeaxxipr55m-ac4c6men2g7xr2a-nojapyorafoorumi-fi.translate.goog/viewforum.php?f=3&sid=56078217ee44c6eb21aa92297b5ffa8e)
    * [discussion of FlevoBike geomentry](https://nojapyorafoorumi.fi/viewtopic.php?f=3&t=4948&start=60)
    * [FWD trike with bottom bracket behind the front wheel](https://4rzcdm4enzemnizaeaxxipr55m-ac4c6men2g7xr2a-nojapyorafoorumi-fi.translate.goog/viewtopic.php?f=3&t=3507&hilit=papan+jopo+url&sid=56078217ee44c6eb21aa92297b5ffa8e)
    * [twist-chain cargo trike](https://4rzcdm4enzemnizaeaxxipr55m-ac4c6men2g7xr2a-nojapyorafoorumi-fi.translate.goog/viewforum.php?f=3&sid=56078217ee44c6eb21aa92297b5ffa8e)
    * [simple no-weld MBB bike](https://nojapyorafoorumi.fi/viewtopic.php?f=3&t=6672&sid=01d47a1a5ddc6828d371627d4bd60f6d)
* [tilting vehicles south africa](http://tilting.org.za) - focus on free-to-caster tiliting vehicles
* [Bicycle Geometry](http://nightrider.xf.cz/geometry.htm), among other topics, courtesy of NightRider
* [bamboo 'bent](https://www.youtube.com/watch?v=MAKrl55m-To) - French, there's probably a thread on velorizontal

### Bike Routes & Maps
* [komoot](https://www.komoot.com/) - apparently has been used by some suntrippers?

### E-Bikes, esp. with solar
* [GRIN Tech's Sun Trip tandem](https://endless-sphere.com/forums/viewtopic.php?f=6&t=93482)
* [Solar Catrike Road conversion, with details on all the components used](https://endless-sphere.com/forums/viewtopic.php?f=6&t=111336)
* [lightweight rigid solar panels](https://www.lightleafsolar.com/)
* [solar ebike build for round-the-world trip](https://endless-sphere.com/forums/viewtopic.php?f=6&t=94721&start=75) - sun tracking trailer; really a lot of research went into this build.  [blog here](https://solare.bike/) with lots of good info on matching battery, charge controller, and panel.  He's a professional solar installer and gets into the technical details really well.  
* [Sunbeam MAXA](https://shop.sunbeamsystem.com/product/maxa-109w-flush-solar-panel/) - seems like a decent panel, not super-high-priced but not a cheapo either
* [PingBattery](https://www.pingbattery.com/) sells LiFePO4 packs and comes recommended.  About $1/Wh
* [LiGo battery](https://ebikes.ca/shop/electric-bicycle-parts/batteries/36v-ligo-battery.html) from ebikes.ca is modular and has really good protections for current, voltage, temp, etc.

### Sail
* [self-trimming wing sails](https://www.notechmagazine.com/2013/02/self-trimming-wingsails.html)

## Internet
* [search tips for dealing w/ seo](https://ask.metafilter.com/351136/Finding-stuff-in-the-age-of-SEO#inline-5019267)
* [virtual conferences](https://www.acm.org/virtual-conferences): [best practices](https://people.clarkson.edu/~jmatthew/acm/VirtualConferences_GuideToBestPractices_CURRENT.pdf) and [platforms](https://docs.google.com/document/d/1LLLniPkf48CCZyG_BNy1ylF2wXNlztqNEOnzNuMQmJc/edit)

## Water
* [biosand water filter](https://www.notechmagazine.com/2016/03/how-to-build-a-biosand-water-filter-using-a-wood-mold.html)

## Electronics
* [simple EMG sensor](https://hackaday.io/project/8823-super-simple-muscle-emg-sensor)

## Fiber & Textiles
* [Nettles for Testiles](http://www.nettlesfortextiles.org.uk/wp/)

## Weather Data
* [environmental data intended for use w/ animal tracking](https://www.movebank.org/cms/movebank-content/env-data) - seems like it could be useful for something?
