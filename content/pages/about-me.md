Title: About April
Lang: en
Summary: 
Slug: about-me
Status: hidden

I've been involved in Bike Collectives and worked at the North Portland Tool Library.  At UC Davis I studied the interplay of technology and culture, and developed a passion for appropriate technology, which nicely complements my love of making and fixing things.  

I avoid traveling by motor vehicles and enjoy building and riding unusual bicycles.  

I'm trans (started transitioning in early 2023). The "D" in "D. April Wick" stands for Darin. I don't consider it a deadname; it just isn't the part of my name I go by these days. Call me April.  Pronouns: She/Her