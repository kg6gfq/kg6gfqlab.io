Title: About This Site
Lang: en
Summary: Not a solar website yet, but ready to become one!
Slug: about-site
Status: hidden

This site is generated with [Pelican](https://blog.getpelican.com/) and currently hosted on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

The theme is a modified version of the [Solar Theme](https://github.com/lowtechmag/solar) used on [solar.lowtechmagazine.com](https://solar.lowtechmagazine.com/).  (More about the solar version of the Low Tech Magazine Website [here](https://homebrewserver.club/low-tech-website-howto.html) and [here](https://github.com/lowtechmag/solar/wiki/Solar-Web-Design).)

To minimize server load and network traffic (and thus minimize energy usage), the site initially loads with dithered versions of all images.  Full-color, full-resolution versions of the images are available by clicking the "Load Full Image" button below and to the left of each image.  

At some point I hope to move this site to a solar- and/or human-powered server of my own.  
