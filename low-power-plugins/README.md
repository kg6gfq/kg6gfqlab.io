# low-power-plugins
Pelican plugins for the low-power theme; modified slightly from https://github.com/lowtechmag/solar-plugins; mostly to make the following changes:
1. Optionally add a "Load Full Image" button below dithered images in posts and pages that replaces the dithered image a full-resolution, full-color, and full-page-width version.
2. Make images height-responsive so that text is always visible above or below dithered images (full images may be taller than the viewport, though)
