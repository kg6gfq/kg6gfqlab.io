#!/usr/bin/env python
# -*- coding: utf-8 -*- #

#adds advanced dithering effect to images
# Copyright (C) 2018  Roel Roscam Abbing

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import os, logging, imghdr
from pelican import signals
from bs4 import BeautifulSoup
logger = logging.getLogger(__name__)

try:
    from PIL import Image
    import hitherdither
    enabled = True
except:
    logging.warning("Unable to load PIL or hitherdither, disabling thumbnailer")
    enabled = False

DEFAULT_IMAGE_DIR = "images"
DEFAULT_DITHER_DIR = "dithers"
DEFAULT_RESIZE_DIR = "resizes"
DEFAULT_THRESHOLD = [96, 96, 96] # this sets the contrast of the final image, rgb
DEFAULT_TRANSPARENCY= False
DEFAULT_TRANSPARENT_COLOR = [(125,125,125)]
DEFAULT_DITHER_PALETTE = [(25,25,25), (75,75,75),(125,125,125),(175,175,175),(225,225,225),(250,250,250)] # 6 tone palette\
DEFAULT_RESIZE_OUTPUT = True
DEFAULT_DEDITHER = False
DEFAULT_MAX_SIZE = (800,800)

#11 tone palette, heavier, more detail, less visible dither pattern
#[(0,0,0),(25,25,25),(50,50,50),(75,75,75),(100,100,100),(125,125,125),(150,150,150),(175,175,175),(200,200,200),(225,225,225),(250,250,250)]

#3 tone palette, lighter, heavier dithering effect
# [(0,0,0), (125,125,125), (250,250,250)]


def _image_path(pelican):
    return os.path.join(pelican.settings['PATH'],
        pelican.settings.get("IMAGE_PATH", DEFAULT_IMAGE_DIR)).rstrip('/')

def _out_path(pelican):
    return os.path.join(pelican.settings['OUTPUT_PATH'],
                         pelican.settings.get('DITHER_DIR', DEFAULT_DITHER_DIR))

def _resize_path(pelican):
    return os.path.join(pelican.settings['OUTPUT_PATH'],
                         pelican.settings.get('RESIZE_DIR', DEFAULT_RESIZE_DIR))

def dither(pelican):
    global enabled
    if not enabled:
        return

    in_path = _image_path(pelican)
    out_path = _out_path(pelican)
    resize_path = _resize_path(pelican)

    transparency = pelican.settings.get("TRANSPARENCY",DEFAULT_TRANSPARENCY)

    if not os.path.exists(out_path):
        os.mkdir(out_path)
    
    if not os.path.exists(resize_path):
        os.mkdir(resize_path)

    for dirpath, dirs, filenames in os.walk(in_path):
        for filename in filenames:
            this_out_path = os.path.join(out_path, os.path.relpath(dirpath, in_path))
            if not os.path.exists(this_out_path):
                os.mkdir(this_out_path)
            
            this_resize_path = os.path.join(resize_path, os.path.relpath(dirpath, in_path))
            if not os.path.exists(this_resize_path):
                os.mkdir(this_resize_path)
            
            file_, ext = os.path.splitext(filename)
            fn= os.path.join(dirpath,filename)
            of = os.path.join(this_out_path, filename.replace(ext,'.png'))
            of_resized = os.path.join(this_resize_path, filename.replace(ext,'.png'))

            if not os.path.exists(of) and imghdr.what(fn):
                logging.debug("dither plugin: dithering {}".format(fn))

                img= Image.open(fn).convert('RGB')

                resize = pelican.settings.get('RESIZE', DEFAULT_RESIZE_OUTPUT)
                dedither = pelican.settings.get('DEDITHER', DEFAULT_DEDITHER)

                if resize:
                    image_size = pelican.settings.get('SIZE', DEFAULT_MAX_SIZE)
                    img.thumbnail(image_size, Image.LANCZOS)
                    if dedither:
                        img.save(of_resized, optimize=True)
            
                palette = hitherdither.palette.Palette(pelican.settings.get('DITHER_PALETTE', DEFAULT_DITHER_PALETTE))
            
                threshold = pelican.settings.get('THRESHOLD', DEFAULT_THRESHOLD)
            
                img_dithered = hitherdither.ordered.bayer.bayer_dithering(img, palette, threshold, order=8) #see hither dither documentation for different dithering algos

                img_dithered.save(of, optimize=True)
    #logging.debug(calculate_savings(in_path,out_path))

def parse_for_images(instance):
    #based on better_figures_and_images plugin by @dflock, @phrawzty,@if1live,@jar1karp,@dhalperi,@aqw,@adworacz
    #https://github.com/getpelican/pelican-plugins/blob/master/better_figures_and_images/better_figures_and_images.py
    global DEFAULT_IMAGE_DIR
    global DEFAULT_DITHER_DIR
    global DEFAULT_RESIZE_DIR
    global enabled

    if not enabled:
        return

    image_dir = instance.settings.get("IMAGE_PATH", DEFAULT_IMAGE_DIR)
    dither_dir = instance.settings.get('DITHER_DIR', DEFAULT_DITHER_DIR)
    resize_dir = instance.settings.get('RESIZE_DIR', DEFAULT_RESIZE_DIR)
    
    resize = instance.settings.get('RESIZE', DEFAULT_RESIZE_OUTPUT)
    dedither = instance.settings.get('DEDITHER', DEFAULT_DEDITHER)

    if instance._content is not None:
        content = instance._content
        soup = BeautifulSoup(content, 'html.parser')
        for img in soup(['img', 'object']):
            fullsize_src = img['src']
            fn, ext = os.path.splitext(img['src'])
            
            if ext.startswith('.'):
                img['src'] = img['src'].replace(ext, '.png') 
            img['src'] = img['src'].replace(image_dir,dither_dir)
            
            if dedither:
                dedither_src = img['src'].replace(dither_dir,image_dir)
                dedither_link = soup.new_tag("button", *{'class':'dedither'}, onclick="this.parentNode.previousSibling.src='%s'; this.parentNode.parentNode.className = 'fullimage'; this.className += 'hidden'" % fullsize_src)
                dedither_link.string = "Load Full Image"
                img.insert_after(dedither_link)
                #img.parentNode.insertBefore(dedither_link, img.nextSibling)    
                #if resize:
                 #   resize_link = soup.new_tag("p")
                  #  resize_link.string = "resize not implemented"
                   # img.parent.append(resize_link)
                # logger.debug('dither plugin: rewrote image source to {}'.format(img['src']))
        instance._content = soup.decode()
        
        # TODO:
        # switch from altering the src to inserting a second img and making the first one invisible or hidden somehow, so it won't get re-requested if/when someone wants to re-dither
        # implement the "view full size" with a really basic lightbox
        # choose appropriate elements for dedither and full size to facilitate styling and theming 

#<p class="img"><img id="alutrike" alt="Bolted Aluminum Trike" src="./dithers/alutrike.png"/><span class="caption"></span>
#<button type="button" onclick="document.getElementById('alutrike').src='./images/alutrike.jpg'">de-dither</button></p>

#<p class="img"><img alt="Bolted Aluminum Trike" src="./dithers/alutrike.png"/><span class="caption"></span></p>
#<p onclick="this.previousSibling.firstChild.src='./images/alutrike.jpg'">de-dither</p>

def register():
    signals.finalized.connect(dither)
    signals.content_object_init.connect(parse_for_images)
